package sample;


import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.util.List;
import java.nio.charset.StandardCharsets;

/**
 * Created by moshiko on 6/28/2016.
 */
public class ErrorCheckDBAdaptar {
    private DBConnector dbc;
    private String errorType;
    public ErrorCheckDBAdaptar(String serverFilePath){
        Charset charset=StandardCharsets.UTF_8;
        Path path = Paths.get(serverFilePath);
        try {
            List<String> lines = Files.readAllLines(path, charset);

            this.dbc=new DBConnector(lines.get(1),lines.get(2),lines.get(0));
            if(!this.dbc.connect())
                System.out.println("failed to connect");
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    public ResultSet send(String sqlcmd){
        String[] array = sqlcmd.split(" ");
        boolean flag=false;
        ResultSet rs=null;
        if(array[0].equals("SELECT"))
        {
            /*
            flag = check errors for select command(array)
        */
        }
        if(array[0].equals("DESC"))
        {
            /*
            flag = check errors for DESC command(array)
        */
        }
        if(array[0].equals("SHOW"))
        {
            /*
            flag = check errors for SHOW command(array)
        */
        }
        if(!flag) {
            this.errorType = "WRONG QUERY STRUCTURE";
            return null;
        }
        else {
            rs = this.dbc.send(sqlcmd);
            if (rs == null) {
                this.errorType = "LOGICAL ERROR";
                return null;
            }
            return rs;
        }

    }

    public boolean update(String sql){
        /*
        check for syntax error



        if(error)
        {
        this.errorType = "WRONG QUERY STRUCTURE";
        return false;
        }
        */
        boolean f=this.dbc.update(sql);
        if(!f)
        {
            this.errorType = "LOGICAL ERROR";
            return false;
        }
        return true;
    }


}

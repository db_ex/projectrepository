package sample;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.*;
import java.util.List;

/**
 * Created by moshiko on 6/22/2016.
 */
public class DBConnector {
    private String user;
    private String pass;
    private String dbClass;
    private String dbDriver;
    private Connection conn;
    private String lastErrorMsg;

    public DBConnector(String user,String pass,String dbDriver){
        this.user =  user;
        this.pass = pass;
        this.dbDriver = dbDriver;
        this.dbClass = "com.mysql.jdbc.Driver";
        this.conn=null;
        this.lastErrorMsg="";
    }

    public DBConnector(String serverFilePath){
        Charset charset= StandardCharsets.UTF_8;
        Path path = Paths.get(serverFilePath);
        try {
            List<String> lines = Files.readAllLines(path, charset);

            this.user=lines.get(1);
            this.pass=lines.get(2);
            this.dbDriver=lines.get(0);
            this.dbClass = "com.mysql.jdbc.Driver";
            this.conn=null;
            this.lastErrorMsg="";
        } catch (IOException e) {
            this.user=null;
            this.pass=null;
            this.dbDriver=null;
            this.dbClass = null;
            this.conn=null;
            this.lastErrorMsg=e.getMessage();
        }
    }

    /*
    connect to database server
    return false if failed
     */
    public boolean connect() {
        boolean done = false;
        //load driver
        try {
            Class.forName(dbClass).newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            this.lastErrorMsg= ex.getMessage();
            return done;
        }
        // Connection
        try {
            conn = DriverManager.getConnection(dbDriver, user, pass);
            done = true;
        } catch (SQLException ex) {
            this.lastErrorMsg= ex.getMessage();
        }
        return done;
    }

    /*
    send a ddl request to data base
    return false if the request failed
    return true if request succeeded
     */
    public boolean update(String sql){
        Statement stmt = null;
        boolean returnflag=false;
        try{
            stmt =conn.createStatement();
            stmt.executeUpdate(sql);
            returnflag=true;
        }
        catch( SQLException e){
            this.lastErrorMsg=e.getMessage();
        }
        return returnflag;

    }


/*
send a dml request to data base
return null if failed or a resultset for the request
 */
    public ResultSet send(String sql){
        Statement stmt = null;

        ResultSet rs=null;
        try{
            stmt =conn.createStatement();


        rs = stmt.executeQuery(sql);
    } catch( SQLException e){
            this.lastErrorMsg=e.getMessage();
        }
        return rs;
        }



    public String getLastError(){
      return this.lastErrorMsg;
    }
    public String getLastErrorType(){
        if(this.getLastError().contains("error in your SQL syntax"))
            return "WRONG QUERY STRUCTURE";
        else return "LOGICAL ERROR";
    }


}